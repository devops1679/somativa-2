import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Atividade Somativa 2
        </p>
        <p style={{fontSize: 16}}>
          Uma aplicação base React por: <b>Felipe Holek Damasceno</b>
        </p>
        <p style={{fontSize: 16}}>
          Pontifícia Universidade Católica do Paraná - PUCPR
        </p>
        <p style={{fontSize: 16}}>
          Disciplina DevOps
        </p>
      </header>
    </div>
  );
}

export default App;
