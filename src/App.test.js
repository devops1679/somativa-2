import { render, screen } from '@testing-library/react';
import App from './App';

test('renderiza o título da atividade', () => {
  render(<App />);
  const linkElement = screen.getByText(/Atividade Somativa 2/i);
  expect(linkElement).toBeInTheDocument();
});
